var app = angular.module("app", ["firebase"]);
var db = firebase.database();

app.controller("MainController", 
function($scope, $firebaseObject, $firebaseAuth){
    $scope.items = [];
    $firebaseObject(db.ref('/items')).$bindTo($scope,'items');

    $scope.auth = $firebaseAuth();

    $scope.socialErr = null;
    $scope.socialLogin = function (provider) {
        $scope.socialErr = null;
        $scope.auth.$signInWithPopup(provider)
            .then(function (firebaseUser) {
                var uid = firebaseUser.user.uid;
                var newUser = {
                    name: firebaseUser.user.displayName
                };
                db.ref('users/' + uid).update(newUser);
            })
            .catch(function(err){
                $scope.socialErr = err.message;
            });
    };

    $scope.user = {};

    $scope.logout = function () {
        $scope.auth.$signOut();
    };

    $scope.auth.$onAuthStateChanged(function (user) {
        if (user) {
            var ref = db.ref('users/' + user.uid);
            $scope.authUserObj = $firebaseObject(ref);
            $scope.authUserObj.$bindTo($scope, 'user');
        }
        else {
            if ($scope.authUserObj) $scope.authUserObj.$destroy();
            $scope.user = null;
        }
    });








    // NOTIFICATIONS
    $scope.notToken = null;

    document.addEventListener("deviceready", function(){

        window.FirebasePlugin.getToken(function(token) {
            $scope.notToken = token;
            $scope.$apply();
        }, function(error) {
            $scope.notToken = error;
            $scope.$apply();
        });

    }, false);
    

    $scope.subTopic = function(){
        window.FirebasePlugin.subscribe($scope.topic);
    };

});