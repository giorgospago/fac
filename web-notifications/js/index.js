var app = angular.module("app", ["firebase"]);
var db = firebase.database();

app.run(function($rootScope){
    $rootScope.messaging = firebase.messaging();
    $rootScope.messaging.requestPermission()
    .then(function(){
        return $rootScope.messaging.getToken();
    })
    .then(function(token){
        console.log(token);
    })
    .catch(function(){
        console.log('TON HPIAME');
    });
});

app.controller("MainController", function($scope, $timeout, $firebaseObject, $rootScope){
    $scope.title = "PAOK";
    $scope.items = [];
    $firebaseObject(db.ref('/items')).$bindTo($scope,'items');

    $scope.not = null;
    
    $rootScope.messaging
    .onMessage(function(payload) {
        $scope.not = payload.notification;
        $scope.$apply();


        $timeout(function(){
            $scope.not = null;
        }, 3000);
    });
});
