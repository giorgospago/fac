# FAC (Firebase Angular Cordova)

## Install

### Step 1
```sh
git clone git@bitbucket.org:giorgospago/fac.git
npm i
cordova platform add browser android
```

### Step 2
Go to `/www/js/firebaseConfig.js` and fill your app data.

### Step 3
#### Run browser
```sh
cordova run browser
```
#### Run Android
```sh
cordova run android
```

## Push Notifications
### Install firebase plugin
```sh
cordova plugin add cordova-plugin-firebase --save
```